// Fill out your copyright notice in the Description page of Project Settings.
#include "ISTYCharacter.h"
#include "ISTY.h"
#include "ISTYFireLeft.h"
#include "ISTYFireRight.h"
#include "Engine.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
//#include "MotionControllerComponent.h"
//#include "XRMotionControllerBase.h"

// Sets default values
AISTYCharacter::AISTYCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a first person camera component.
	// Attach the camera component to our capsule component.
	// Position the camera slightly above the eyes.
	// Allow the pawn to control camera rotation.
	ISTYCameraComponent = CreateAbstractDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	ISTYCameraComponent->SetupAttachment(GetCapsuleComponent());
	ISTYCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f + BaseEyeHeight));
	ISTYCameraComponent->bUsePawnControlRotation = true;

	// Create a first person mesh component for the owning player.
	// Only the owning player sees this mesh.
	// Attach the FPS mesh to the FPS camera.
	// Disable some environmental shadowing to preserve the illusion of having a single mesh.
	ISTYMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	ISTYMesh->SetOnlyOwnerSee(true);
	ISTYMesh->SetupAttachment(ISTYCameraComponent);
	ISTYMesh->bCastDynamicShadow = false;
	ISTYMesh->CastShadow = false;
	ISTYMesh->RelativeRotation = FRotator(2.0f, -15.0f, 5.0f);
	ISTYMesh->RelativeLocation = FVector(0, 0, -160.0f);


	GetCharacterMovement()->GravityScale = 2.0f;
	GetCharacterMovement()->JumpZVelocity = 1200.0f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	GetCharacterMovement()->AirControl = 10.0f;
	GetCharacterMovement()->GroundFriction = 100;

	// Create a gun mesh component
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetupAttachment(RootComponent);


	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// beamcomponent
	BeamComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BeamComp"));
	BeamComp->SetupAttachment(FP_Gun);
	BeamComp->bAutoActivate = false;

}

// Called when the game starts or when spawned
void AISTYCharacter::BeginPlay()
{
	Super::BeginPlay();
	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(ISTYMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("Rifle"));

	if (GEngine)
	{
		// Put up a debug message for five seconds. The -1 "Key" value (first argument) indicates that we will never need to update or refresh this message.
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("We are using FPSCharacter."));
	}
	// The owning player doesn't see the regular (third-person) body mesh.
	GetMesh()->SetOwnerNoSee(true);

	
}

// Called every frame
void AISTYCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AISTYCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Set up "movement" bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &AISTYCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AISTYCharacter::MoveRight);

	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &AISTYCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AISTYCharacter::AddControllerPitchInput);
	

	// Set up "action" bindings.
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AISTYCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AISTYCharacter::StopJump);

	//Projectile
	PlayerInputComponent->BindAction("FireLeft", IE_Pressed, this, &AISTYCharacter::FireLeft);
	PlayerInputComponent->BindAction("FireRight", IE_Pressed, this, &AISTYCharacter::FireRight);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AISTYCharacter::SprintStart);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AISTYCharacter::SprintStop);
}

// Called to bind functionality to input
void AISTYCharacter::MoveForward(float Value)
{
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void AISTYCharacter::MoveRight(float Value)
{
	// Find out which way is "right" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

void AISTYCharacter::StartJump()
{
	bPressedJump = true;

	// try and play the sound if specified
	if (SoundJumpStart != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, SoundJumpStart, GetActorLocation());
	}


}

void AISTYCharacter::StopJump()
{
	bPressedJump = false;


	
}

void AISTYCharacter::FireLeft()
{

	if (GEngine)
	{
		// Put up a debug message for five seconds. The -1 "Key" value (first argument) indicates that we will never need to update or refresh this message.
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("FIRE LEFT."));
	}
	// Attempt to fire a projectile.
	if (ProjectileClassLeft)
	{
		

		FVector MuzzleLocation = FP_Gun->GetSocketLocation("MuzzleSocket");
		FRotator MuzzleRotation = FP_Gun->GetSocketRotation("MuzzleSocket");

		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride =
		ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		ActorSpawnParams.Owner = GetOwner();

		// spawn the projectile at the muzzle
		GetWorld()->SpawnActor<AISTYFireLeft>(ProjectileClassLeft, MuzzleLocation, MuzzleRotation, ActorSpawnParams);
			
		
	}
	// try and play the sound if specified
	if (FireSoundLeft != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSoundLeft, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimationLeft != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = ISTYMesh->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->PlaySlotAnimationAsDynamicMontage(FireAnimationLeft, "Arms", 0.0f);
		}
	}
	
}

void AISTYCharacter::FireRight()
{

	if (GEngine)
	{
		// Put up a debug message for five seconds. The -1 "Key" value (first argument) indicates that we will never need to update or refresh this message.
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("FIRE RIGHT."));
	}
	// Attempt to fire a projectile.
	if (ProjectileClassRight)
	{
		FVector MuzzleLocation = FP_Gun->GetSocketLocation("MuzzleSocket");
		FRotator MuzzleRotation = FP_Gun->GetSocketRotation("MuzzleSocket");

		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride =
			ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		ActorSpawnParams.Owner = GetOwner();

		// spawn the projectile at the muzzle
		GetWorld()->SpawnActor<AISTYFireRight>(ProjectileClassRight, MuzzleLocation, MuzzleRotation, ActorSpawnParams);
	}
	// try and play the sound if specified
	if (FireSoundRight != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSoundRight, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimationRight != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = ISTYMesh->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->PlaySlotAnimationAsDynamicMontage(FireAnimationRight, "Arms", 0.0f);
		}
	}

}


void AISTYCharacter::SprintStart()
{
	GetCharacterMovement()->MaxWalkSpeed = 1500.f;
	GetCharacterMovement()->MaxAcceleration = 100000.f;

	// try and play the sound if specified
	if (SoundRun)
	{
		UGameplayStatics::PlaySoundAtLocation(this, SoundRun, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (AnimationRun)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = ISTYMesh->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->PlaySlotAnimationAsDynamicMontage(AnimationRun, "Arms", 0.0f);
		}
	}
	
}

void AISTYCharacter::SprintStop()
{
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	
	
}


void AISTYCharacter::AddNewBeam(FVector Point1, FVector Point2)
{

	

	BeamComp->GetSocketLocation("MuzzleSocket");
	BeamComp->GetSocketLocation("MuzzleSocket");
}

