// Fill out your copyright notice in the Description page of Project Settings.

#include "ISTYFireLeft.h"
#include "ISTYCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "ISTY.h"

// Sets default values
AISTYFireLeft::AISTYFireLeft()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation.
	// Set the sphere's collision radius.
	// Set the root component to be the collision component.
	CollisionComponentLeft = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComponentLeft->BodyInstance.SetCollisionProfileName(TEXT("FireLeft"));
	CollisionComponentLeft->OnComponentHit.AddDynamic(this, &AISTYFireLeft::OnHit);
	
	// Set the sphere's collision radius.
	CollisionComponentLeft->InitSphereRadius(25.0f);

	RootComponent = CollisionComponentLeft;

	// Use this component to drive this projectile's movement.
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponentLeft);
	ProjectileMovementComponent->InitialSpeed = 3000.0f;
	ProjectileMovementComponent->MaxSpeed = 3000.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = true;
	ProjectileMovementComponent->Bounciness = 0.5f;


	InitialLifeSpan = 10.0f;

	ProjectileMovementComponent->ProjectileGravityScale = 0;

	//Replication
	SetReplicates(true);
	SetReplicateMovement(true);

}

// Called when the game starts or when spawned
void AISTYFireLeft::BeginPlay()
{
	Super::BeginPlay();

	bool OwnedLocally = IsOwnedBy(UGameplayStatics::GetPlayerController(this, 0));
	
}

// Called every frame
void AISTYFireLeft::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Function that initializes the projectile's velocity in the shoot direction.
void AISTYFireLeft::FireInDirection(const FVector& ShootDirection)
{
	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}


// Function that is called when the projectile hits something.
void AISTYFireLeft::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	AISTYCharacter* Char = Cast<AISTYCharacter>(OtherActor);

	TSubclassOf<UDamageType> P;

	UGameplayStatics::ApplyRadialDamage(GetWorld(), 1, GetActorLocation(), 100.0f, P, TArray<AActor*>(), this, (AController*)GetOwner(), true, ECC_Visibility);

	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComponent != NULL) && OtherComponent->IsSimulatingPhysics())
	{
		OtherComponent->AddImpulseAtLocation(ProjectileMovementComponent->Velocity * 100.0f, Hit.ImpactPoint);

		
	}
	Destroy();
}