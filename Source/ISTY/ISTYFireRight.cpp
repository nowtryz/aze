#include "ISTYFireRight.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "ISTYCharacter.h"
#include "ISTY.h"
// Sets default values
AISTYFireRight::AISTYFireRight()
{
	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation.
	CollisionComponentRight = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComponentRight->BodyInstance.SetCollisionProfileName(TEXT("FireRight"));
	CollisionComponentRight->OnComponentHit.AddDynamic(this, &AISTYFireRight::OnHit);

	// Set the sphere's collision radius.
	CollisionComponentRight->InitSphereRadius(50.0f);

	// Set the root component to be the collision component.
	RootComponent = CollisionComponentRight;

	// Use this component to drive this projectile's movement.
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponentRight);
	ProjectileMovementComponent->InitialSpeed = 3000.0f;
	ProjectileMovementComponent->MaxSpeed = 3000.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = true;
	ProjectileMovementComponent->Bounciness = 0.3f;

	// Die after 3 seconds.
	InitialLifeSpan = 10.0f;

	//Replication
	SetReplicates(true);
	SetReplicateMovement(true);

}

// Called when the game starts or when spawned.
void AISTYFireRight::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame.
void AISTYFireRight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Function that initializes the projectile's velocity in the shoot direction.
void AISTYFireRight::FireInDirection(const FVector& ShootDirection)
{
	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}


// Function that is called when the projectile hits something.
void AISTYFireRight::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	AISTYCharacter* Char = Cast<AISTYCharacter>(OtherActor);

	TSubclassOf<UDamageType> P;

	UGameplayStatics::ApplyRadialDamage(GetWorld(), 1, GetActorLocation(), 100.0f, P, TArray<AActor*>(), this, (AController*)GetOwner(), true, ECC_Visibility);

	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComponent != NULL) && OtherComponent->IsSimulatingPhysics())
	{
		OtherComponent->AddImpulseAtLocation(ProjectileMovementComponent->Velocity * 100.0f, Hit.ImpactPoint);

		
	}
	Destroy();
}