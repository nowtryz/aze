// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelPlayerControllerCPP.h"
#include "ISTYGameModeBase.h"


void ALevelPlayerControllerCPP::PauseMenu()
{
	if (PauseMenuWidget != nullptr)
	{
		auto GameMode = Cast<AISTYGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode) GameMode->ChangeMenuWidget(DefaultPauseMenu);
	}
}

void ALevelPlayerControllerCPP::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Pause", IE_Pressed, this, &ALevelPlayerControllerCPP::PauseMenu);
}

void ALevelPlayerControllerCPP::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameOnly());
	if (DefaultPauseMenu) PauseMenuWidget = CreateWidget<UMenuUserWidget>(GetWorld(), DefaultPauseMenu);
	if (ControlsMenu) ControlsMenuWidget = CreateWidget<UMenuUserWidget>(GetWorld(), ControlsMenu);
	if (GraphicsMenu) GraphicsMenuWidget = CreateWidget<UMenuUserWidget>(GetWorld(), GraphicsMenu);

	if (ControlsMenuWidget) ControlsMenuWidget->SetParentUserWidget(DefaultPauseMenu);
	if (GraphicsMenuWidget) GraphicsMenuWidget->SetParentUserWidget(DefaultPauseMenu);
}

UMenuUserWidget* ALevelPlayerControllerCPP::GetControlsMenuWidget()
{
	return ControlsMenuWidget;
}

UMenuUserWidget* ALevelPlayerControllerCPP::GetGraphicsMenuWidget()
{
	return GraphicsMenuWidget;
}
