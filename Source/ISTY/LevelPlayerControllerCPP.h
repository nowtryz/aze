// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuUserWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "GameFramework/PlayerController.h"
#include "LevelPlayerControllerCPP.generated.h"

/**
 * 
 */
UCLASS()
class ISTY_API ALevelPlayerControllerCPP : public APlayerController
{
	GENERATED_BODY()

protected:
	// Input variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Actions Binding")
		TSubclassOf<class UMenuUserWidget> DefaultPauseMenu;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Actions Binding")
		TSubclassOf<class UMenuUserWidget> ControlsMenu;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Actions Binding")
		TSubclassOf<class UMenuUserWidget> GraphicsMenu;

	// Input functions
	void PauseMenu();

	// Widgets
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Linked Widgets")
		UMenuUserWidget* PauseMenuWidget;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Linked Widgets")
		UMenuUserWidget* ControlsMenuWidget;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Linked Widgets")
		UMenuUserWidget* GraphicsMenuWidget;
	
protected:
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;


public:
	UMenuUserWidget* GetControlsMenuWidget();
	UMenuUserWidget* GetGraphicsMenuWidget();
};
