// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuUserWidget.h"

UMenuUserWidget::UMenuUserWidget(const FObjectInitializer & ObjectIn...) : UUserWidget(ObjectIn), PauseGameOnConstruct(false), ParentUserWidgetClass(NULL) {}

void UMenuUserWidget::NativeConstruct()
{
	Super::NativeConstruct();
	PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	GameMode = Cast<AISTYGameModeBase>(GetWorld()->GetAuthGameMode());

	UE_LOG(LogTemp, Warning, TEXT("pause loaded"));

	if (PlayerController)
	{
		PlayerController->SetInputMode(FInputModeUIOnly());
		PlayerController->bShowMouseCursor = true;
		PlayerController->bEnableClickEvents = true;
		PlayerController->bEnableMouseOverEvents = true;
		PlayerController->SetPause(PauseGameOnConstruct || PlayerController->IsPaused());
	}
}

void UMenuUserWidget::PrepareControllerForExit(const bool ResumeGame)
{
	if (PlayerController)
	{
		PlayerController->SetInputMode(FInputModeGameOnly());
		PlayerController->bShowMouseCursor = false;
		PlayerController->bEnableClickEvents = false;
		PlayerController->bEnableMouseOverEvents = false;
		PlayerController->SetPause(!ResumeGame);
	}
}

void UMenuUserWidget::ExitMenu(const bool ResumeGame)
{
	RemoveFromViewport();
	this->PrepareControllerForExit(ResumeGame);
}

UMenuUserWidget * UMenuUserWidget::SwitchToWidget(TSubclassOf<UMenuUserWidget> NewWidgetClass, const bool ResumeGame)
{
	UUserWidget* NewWidget = nullptr;
	UMenuUserWidget* NewMenuWidget = nullptr;

	this->PrepareControllerForExit(ResumeGame);
	if (GameMode) NewWidget = GameMode->ChangeMenuWidget(NewWidgetClass);
	if (NewWidget) {
		NewMenuWidget = Cast<UMenuUserWidget>(NewWidget);
		NewMenuWidget->SetParentUserWidget(this->GetClass());
		return NewMenuWidget;
	}
	else return nullptr;
}

UMenuUserWidget * UMenuUserWidget::SwitchToParent(const bool ResumeGame)
{
	return this->SwitchToWidget(ParentUserWidgetClass, ResumeGame);
}

void UMenuUserWidget::SetParentUserWidget(TSubclassOf<UMenuUserWidget> ParentWidgetClass)
{
	this->ParentUserWidgetClass = ParentWidgetClass;
}

