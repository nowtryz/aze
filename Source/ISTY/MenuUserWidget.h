// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ISTYGameModeBase.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "MenuUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class ISTY_API UMenuUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
		UMenuUserWidget(const FObjectInitializer & ObjectIn...);

protected:
	APlayerController* PlayerController;
	AISTYGameModeBase* GameMode;
	TSubclassOf<UMenuUserWidget> ParentUserWidgetClass;

public:
	virtual void NativeConstruct() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UMG Menu|Properties")
		bool PauseGameOnConstruct;

	/** Close this Menu */
	UFUNCTION(BlueprintCallable, Category = "UMG Menu|Actions")
		void ExitMenu(const bool ResumeGame);

	/** Close this widget, open the selected one and set this one to its parent*/
	UFUNCTION(BlueprintCallable, Category = "UMG Menu|Actions")
		UMenuUserWidget* SwitchToWidget(TSubclassOf<UMenuUserWidget> NewWidgetClass, const bool ResumeGame);

	/** Close this menu and reopen its parent */
	UFUNCTION(BlueprintCallable, Category = "UMG Menu|Actions")
		UMenuUserWidget* SwitchToParent(const bool ResumeGame);

public:
	UFUNCTION(BlueprintCallable, Category = "UMG Menu|Actions")
		void SetParentUserWidget(TSubclassOf<UMenuUserWidget> ParentWidgetClass);

private:
	void PrepareControllerForExit(const bool ResumeGame);
};
